﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Jake.Collections.Generic;

namespace Jake.Csv
{
	public static class CsvConverter
	{
		public static Matrix<string> StringToMatrix(string value)
		{
			var matrix = new Matrix<string>();

			var rows = value.Split('\n');
			foreach (var row in rows)
			{
				if (row.Length > 0)
					matrix.InsertRow(row.TrimEnd('\r').Split(','));
			}
			
			return matrix;
		}

		public static string MatrixToString(Matrix<string> value)
		{
			var rows = new string[value.rowCount];
			for (int i = 0; i < value.rowCount; ++i)
			{
				rows[i] = string.Join(",", value.GetRow(i));
			}

			return string.Join("\n", rows);
		}
	}
}
