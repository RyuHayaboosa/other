﻿using System;
using System.Collections.Generic;

namespace Jake.Collections.Generic
{
	/// <summary>
	/// Generic matrix collection
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class Matrix<T>
	{
		// Private fields
		private List<List<T>> cells = new List<List<T>>();

		// Public properties
		public int rowCount
		{
			get
			{
				return cells.Count;
			}
		}

		public int columnCount
		{
			get
			{
				return cells[0].Count;
			}
		}

		// Constructors
		public Matrix() : this(0, 0)
		{

		}

		public Matrix(int rows, int columns)
		{
			for (int i = 0; i < rows; ++i)
			{
				cells.Add(new List<T>(columns));
			}
		}

		// Public methods
		// Cell
		public T GetCell(int row, int column)
		{
			CheckCell(row, column);

			return cells[row][column];
		}

		public void SetCell(int row, int column, T value)
		{
			CheckCell(row, column);

			cells[row][column] = value;
		}

		// Row
		public T[] GetRow(int row)
		{
			CheckRow(row);

			var cells = new T[columnCount];
			for (int i = 0; i < columnCount; ++i)
			{
				cells[i] = GetCell(row, i);
			}

			return cells;
		}

		public void SetRow(int row, T[] values)
		{
			CheckRow(row);

			for (int i = 0; i < columnCount; ++i)
			{
				SetCell(row, i, values[i]);
			}
		}

		public void InsertRow(int row, T[] values)
		{
			if (row > rowCount)
				CheckRow(row);

			if (row == rowCount)
			{
				cells.Add(new List<T>(values));
			}
			else
			{
				cells.Insert(row, new List<T>(values));
			}
		}

		public void InsertRow(T[] values)
		{
			InsertRow(rowCount, values);
		}

		public void InsertRow(int row)
		{
			InsertRow(row, new T[columnCount]);
		}

		public void AddRow()
		{
			InsertRow(rowCount);
		}

		public void DeleteRow(int row)
		{
			CheckRow(row);

			cells.RemoveAt(row);
		}
		
		// Column
		public T[] GetColumn(int column)
		{
			CheckColumn(column);

			var cells = new T[rowCount];
			for (int i = 0; i < rowCount; ++i)
			{
				cells[i] = GetCell(i, column);
			}

			return cells;
		}

		public void SetColumn(int column, T[] values)
		{
			CheckColumn(column);

			for (int i = 0; i < rowCount; ++i)
			{
				SetCell(i, column, values[i]);
			}
		}

		public void InsertColumn(int column, T[] values)
		{
			if (column > columnCount)
				CheckRow(column);

			if (column == columnCount)
			{
				int i = 0;
				foreach (var row in cells)
				{
					row.Add(values[i++]);
				}
			}
			else
			{
				int i = 0;
				foreach (var row in cells)
				{
					row.Insert(column, values[i++]);
				}
			}
		}

		public void InsertColumn(T[] values)
		{
			InsertColumn(columnCount, values);
		}

		public void InsertColumn(int column)
		{
			InsertColumn(column, new T[rowCount]);
		}

		public void AddColumn()
		{
			InsertColumn(columnCount);
		}

		public void DeleteColumn(int column)
		{
			CheckColumn(column);

			foreach (var row in cells)
			{
				row.RemoveAt(column);
			}
		}

		// Private methods
		private void CheckCell(int row, int column)
		{
			CheckRow(row);
			CheckColumn(column);
		}

		private void CheckRow(int row)
		{
			if (row >= rowCount)
				throw new Exception(string.Format("row ({0}) > rowCount ({1})", row, rowCount));
		}

		private void CheckColumn(int column)
		{
			if (column >= columnCount)
				throw new Exception(string.Format("row ({0}) > rowCount ({1})", column, columnCount));
		}
	}
}
