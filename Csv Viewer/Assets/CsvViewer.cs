﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using Jake.Collections.Generic;

namespace Jake.Csv.Unity
{
	public enum Axis { Origin, Row, Column };

	public class CsvViewer : EditorWindow
	{
		[MenuItem("Window/Csv Viewer")]
		static void Init()
		{
			var csvViewer = GetWindow<CsvViewer>("Csv Viewer");
			csvViewer.Initialise();
			csvViewer.Show();
		}

		private TextAsset csvFile;
		private Matrix<string> matrix;

		private HashSet<int> selectedRows		= new HashSet<int>();
		private HashSet<int> selectedColumns	= new HashSet<int>();
		private bool selectAll;

		private Vector2 scrollPos;

		private bool testBool;

		private GUIStyle cellStyle, originStyle;
		private GUILayoutOption[] cellLayout = new GUILayoutOption[] 
		{
			GUILayout.Width(EditorGUIUtility.labelWidth),
			GUILayout.MinWidth(EditorGUIUtility.fieldWidth),
			GUILayout.ExpandWidth(true)
		};

		public void Initialise()
		{
			if (cellStyle == null)
			{
				cellStyle = new GUIStyle(EditorStyles.textField);
				cellStyle.margin = new RectOffset(0, 0, 0, 0);
			}

			if (originStyle == null)
			{
				originStyle = new GUIStyle(EditorStyles.label);
				originStyle.margin = new RectOffset(0, 0, 0, 0);
			}
		}

		void OnGUI()
		{
			var prevCsvFile = csvFile;
			csvFile = EditorGUILayout.ObjectField(csvFile, typeof(TextAsset), false) as TextAsset;

			if (csvFile != prevCsvFile)
			{
				Load();
			}

			GUILayout.BeginHorizontal();
			if (GUILayout.Button("Add Row", EditorStyles.toolbarButton))	AddRow();
			if (GUILayout.Button("Add Column", EditorStyles.toolbarButton)) AddColumn();
			GUILayout.EndHorizontal();

			GUILayout.BeginHorizontal();

			if (selectedRows.Count > 0 || selectedColumns.Count > 0)
			{
				if (GUILayout.Button("Insert", EditorStyles.toolbarButton)) Insert();
				GUILayout.Button("Delet2e", EditorStyles.toolbarButton);
				if (GUILayout.Button("Clear", EditorStyles.toolbarButton))	Clear();
			}

			if (selectedRows.Count > 0 && selectedColumns.Count == 0 || 
				selectedColumns.Count > 0 && selectedRows.Count == 0)
			{
				GUILayout.Button("Copy", EditorStyles.toolbarButton);
				GUILayout.Button("Paste", EditorStyles.toolbarButton);
			}

			GUILayout.EndHorizontal();

			EditorGUILayout.Space();

			// draw
			scrollPos = EditorGUILayout.BeginScrollView(scrollPos);
			DrawMatrix();
			EditorGUILayout.EndScrollView();

			EditorGUILayout.Space();
			GUILayout.FlexibleSpace();

			GUILayout.BeginHorizontal();
			if (GUILayout.Button("Save", EditorStyles.toolbarButton))
			{
				Save();
			}
			
			GUILayout.EndHorizontal();
		}

		private void DrawMatrix()
		{
			if (matrix == null)
				return;
			
			GUILayout.BeginVertical();
			GUILayout.BeginHorizontal();
			
			DrawLabel(Axis.Origin, 0);
			EditorGUILayout.Space();

			for (int i = 0; i < matrix.columnCount; ++i)
			{
				DrawLabel(Axis.Column, i);
			}

			GUILayout.EndHorizontal();

			EditorGUILayout.Space();

			for (int i = 0; i < matrix.rowCount; ++i)
			{
				GUILayout.BeginHorizontal();
				
				DrawLabel(Axis.Row, i);
				EditorGUILayout.Space();
				
				for (int j = 0; j < matrix.columnCount; ++j)
				{
					var value = matrix.GetCell(i, j);
					value = EditorGUILayout.TextField(value, cellStyle, cellLayout);
					matrix.SetCell(i, j, value);
				}

				GUILayout.EndHorizontal();
			}

			GUILayout.EndVertical();
		}
		
		private bool DrawLabel(Axis axis, int element)
		{
			if (axis == Axis.Origin)
			{
				GUILayout.Label("", originStyle, cellLayout);
				return false;
			}
			else
			{
				var label = axis == Axis.Origin ? "" : string.Format("{0}{1}", axis == Axis.Row ? "R" : "C", element);
				return GUILayout.Button(label, cellStyle, cellLayout);
			}
		}

		private void AddRow()
		{
			matrix.AddRow();
		}

		private void AddColumn()
		{
			matrix.AddColumn();
		}

		private void Insert()
		{
			foreach (var row in selectedRows)
			{
				matrix.InsertRow(row);
			}

			foreach (var column in selectedColumns)
			{
				matrix.InsertColumn(column);
			}
		}

		private void Clear()
		{
			// clear rows
			var clearRow = new string[matrix.columnCount];
			for (int i = 0; i < matrix.columnCount; ++i)
				clearRow[i] = "";

			foreach (var row in selectedRows)
			{
				matrix.SetRow(row, clearRow);
			}

			// clear columns
			var clearColumn = new string[matrix.rowCount];
			for (int i = 0; i < matrix.rowCount; ++i)
				clearColumn[i] = "";

			foreach (var column in selectedColumns)
			{
				matrix.SetColumn(column, clearColumn);
			}
		}

		private void Load()
		{
			if (csvFile == null)
				return;

			matrix = CsvConverter.StringToMatrix(csvFile.text);
		}

		private void Save()
		{
			if (matrix == null)
				return;

			var unityPath = Application.dataPath;
			var assetPath = AssetDatabase.GetAssetPath(csvFile);
			var path = unityPath.Remove(unityPath.Length - "Assets".Length) + assetPath;
			
			var text = CsvConverter.MatrixToString(matrix);
			File.WriteAllText(path, text);

			AssetDatabase.Refresh();
		}
	}
}
